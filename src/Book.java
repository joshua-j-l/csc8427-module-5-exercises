public class Book {

    //attributes
    private String bookTitle;
    private String author;
    private String date;

    //constructor
    public Book(String bookTitle, String author, String date) {
        this.bookTitle = bookTitle;
        this.author = author;
        this.date = date;
    }

    //getter
    public String getBookTitle() {
        return bookTitle;
    }

    public String getAuthor() {
        return author;
    }

    public String getDate() {
        return date;
    }

    //setters
    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setDate(String date) {
        this.date = date;
    }

}




