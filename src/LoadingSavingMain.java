//FileReader - class for reading in files stored in text format, including .txt and .csv files
//BufferedReader - a class designed to iterate through a file (or input stream) line-by-line

import java.io.*;

public class LoadingSavingMain {

    public static void main(String[] args) {


//use try as creating a fileReader will throw an exception we need to deal with
        try {
            String csvFile = "data/books.csv";

            // creating a fileReader object, refering to the constructor to add the correct parameter
            //FileNotFoundException will be thrown if we dont use the try block
            FileReader fileReader = new FileReader(csvFile);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            Book[] books = new Book[6];

            String nextLine = bufferedReader.readLine(); // create new varibale to store readLine

            for (int i = 0; i < books.length; i++) {

                nextLine = bufferedReader.readLine(); // call readLine a second time to skip first line of the csv

                if (nextLine != null) {
                    String[] strings = nextLine.split(","); //break up line of text at each "," and store in an array

                    String title = strings[0];
                    String author = strings[1];
                    String date = strings[2];

                    books[i] = new Book(title, author, date); // transfer data from array to book object
                }
            }

            //print list
            for (int i = 0; i < books.length; i++) {
                if (books[i] != null) {
                    System.out.println(books[i].getBookTitle() + " " + books[i].getAuthor() + " " + books[i].getDate());
                } else {
                    break;
                }
            }
            System.out.println(" End of list");

            // add books
            Book bookOne = new Book("titleOne", "authorOne", "dateOne");
            Book bookTwo = new Book("titleTwo", "authorTwo", "dateTwo");
            Book bookThree = new Book("titleThree", "authorThree", "dateThree");

            for (int i = 0; i < books.length; i++) {
                if (books[i] == null) {
                    books[i] = bookOne;
                    break;
                } else {
                }
            }

            for (int i = 0; i < books.length; i++) {
                if (books[i] == null) {
                    books[i] = bookTwo;
                    break;
                } else {
                }
            }

            for (int i = 0; i < books.length; i++) {
                if (books[i] == null) {
                    books[i] = bookThree;
                    break;
                } else {
                }
            }

            //print list
            for (int i = 0; i < books.length; i++) {
                if (books[i] != null) {
                    System.out.println(books[i].getBookTitle() + " " + books[i].getAuthor() + " " + books[i].getDate());
                } else {
                    break;
                }
            }
            System.out.println(" End of list");

            // catch
            //in this example, FileNotFoundException and IOException are thrown

            //Write file
            FileWriter csvWriter = new FileWriter("data/booksoutput.csv");

            //add header rows
            csvWriter.append("Title");
            csvWriter.append(","); // separating by ','
            csvWriter.append("Author");
            csvWriter.append(",");
            csvWriter.append("Date");
            csvWriter.append("\n"); //insert new line

            //add list entry rows
            for (int i = 0; i < books.length; i++){

                csvWriter.append(books[i].getBookTitle());
                csvWriter.append(",");
                csvWriter.append(books[i].getAuthor());
                csvWriter.append(",");
                csvWriter.append(books[i].getDate());
                csvWriter.append("\n");
            }

            csvWriter.flush();
            csvWriter.close();

        } catch (Exception e)                 // Catch ANY Exception that is thrown
        {
            System.out.println(e.getMessage());     // Print the Exception message
            System.out.println(e.getStackTrace());  // Print the Stack Trace
            System.exit(1);                         // (Optional) Exit (with code 1 to indicate an error has occurred)
        }
    }
}
