public class HaikuMain {

    public static void main(String[] args) {

        String [][] strings = {
                { "An", "Old", "Silent", "Pond...","","", "\n"},
                { "A", "frog", "jumps", "into", "the", "pond,", "\n"},
                { "splash!","Silence", "again.", "", "", "","\n"}
        };

        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 7; j++){
                if (j <6) {
                    System.out.print(strings[i][j] + " ");
                }
                else{
                    System.out.print(strings[i][j]);
                }
            }
        }


    }
}
